//
//  ViewController.swift
//  swifty-demo
//
//  Created by Mavin on 10/11/21.
//

import UIKit
import ProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var tableVIew: UITableView!
    
    var articles: [Article] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // initializing the refreshControl
        tableVIew.refreshControl = UIRefreshControl()
        
        // add target to UIRefreshControl
        tableVIew.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        fetch()
    }
    
    @objc func refresh(){
        fetch()
    }
    
    func fetch(){
        ProgressHUD.show()
        Network.shared.fetchArticles { result in
            switch result{
            case .success(let articles):
                self.articles = articles
                self.tableVIew.reloadData()
                ProgressHUD.showSucceed("Get data successfully")
                self.tableVIew.refreshControl?.endRefreshing()
                
            case .failure(let error):
                ProgressHUD.showError(error.localizedDescription)
                self.tableVIew.refreshControl?.endRefreshing()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailArticle" {
            if let desVC = segue.destination as? DetailtViewController, let indexPath = sender as? IndexPath{

                let article = self.articles[indexPath.row]
                
                desVC.article = article
            }
           
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.articles.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "detailArticle", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellArticle", for: indexPath) as! ArticleTableViewCell
        cell.config(article: self.articles[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .destructive, title: "Delete"){ (action,view, completion) in
            
            let alert = UIAlertController(title: "Are you sure to delete?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                let id = self.articles[indexPath.row].id
                Network.shared.deleteArticles(id)
                self.articles.remove(at: indexPath.row)
                self.tableVIew.deleteRows(at: [indexPath], with: .fade)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            completion(true)
        }
        
        let edit = UIContextualAction(style: .normal, title: "Edit"){ (action, view, completion) in
            
            let updateVC = self.storyboard?.instantiateViewController(withIdentifier: "editScreen") as! PostViewController
            
            let article = self.articles[indexPath.row]
            
            updateVC.updateArticle = article
            updateVC.isUpdate = true
            
            self.navigationController?.pushViewController(updateVC, animated: true)
            
        }
        
        let config = UISwipeActionsConfiguration(actions: [delete, edit])
              config.performsFirstActionWithFullSwipe = false
        
        return config
    }
}
